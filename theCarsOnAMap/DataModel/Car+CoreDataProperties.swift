//
//  Car+CoreDataProperties.swift
//  theCarsOnAMap
//
//  Created by AdrianRoseMacBookAir on 29.10.19.
//  Copyright © 2019 apsrose. All rights reserved.
//
//

import Foundation
import CoreData


extension Car {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Car> {
        return NSFetchRequest<Car>(entityName: "Car")
    }

    @NSManaged public var id: String?
    @NSManaged public var modelIdentifier: String?
    @NSManaged public var modelName: String?
    @NSManaged public var name: String?
    @NSManaged public var group: String?
    @NSManaged public var color: String?
    @NSManaged public var series: String?
    @NSManaged public var fuelType: String?
    @NSManaged public var fuellevel: Double
    @NSManaged public var transmission: String?
    @NSManaged public var licensePlate: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var innerCleanliness: String?
    @NSManaged public var carImageUrl: String?

}
