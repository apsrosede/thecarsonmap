//
//  CarImageCache+CoreDataProperties.swift
//  theCarsOnAMap
//
//  Created by AdrianRose on 01.11.19.
//  Copyright © 2019 apsrose. All rights reserved.
//
//

import Foundation
import CoreData


extension CarImageCache {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CarImageCache> {
        return NSFetchRequest<CarImageCache>(entityName: "CarImageCache")
    }

    @NSManaged public var modelIdentifier: String?
    @NSManaged public var imageStore: Data?
    @NSManaged public var carImageUrl: String?

}
