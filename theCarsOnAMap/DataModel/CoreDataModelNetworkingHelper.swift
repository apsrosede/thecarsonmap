//
//  CoreDataModelNetworkingHelper.swift
//  theCarsOnAMap
//
//  Created by AdrianRose on 30.10.19.
//  Copyright © 2019 apsrose. All rights reserved.
//

import Foundation
import CoreData
import MapKit

internal typealias networkDataIntoLocalStoreClosure = (_ retunedCars: [CarRemote]) -> ()
let dataModelHasUpdated = "dataModelHasUpdated"
let dataModelNotification = Notification.Name(dataModelHasUpdated)

class dataToNetworkManager {
    
    public var dataTasksDownloadingCurrently = [URLSessionDataTask]()

    let managedObjectContext : NSManagedObjectContext
    
    init(theContextToSaveInto: NSManagedObjectContext) {
        managedObjectContext = theContextToSaveInto
    }
    
    public func deleteAllData()  {
        
        self.managedObjectContext.performAndWait {
            
            var resultDeleteCars: NSPersistentStoreResult?
            var resultDeleteImagecache: NSPersistentStoreResult?

            
            let fetchCars: NSFetchRequest<NSFetchRequestResult> = Car.fetchRequest()
            let fetchImageCache: NSFetchRequest<NSFetchRequestResult> = CarImageCache.fetchRequest()
            
            let requestdeleteCars = NSBatchDeleteRequest(fetchRequest: fetchCars)
            let requestdeleteImageCache = NSBatchDeleteRequest(fetchRequest: fetchImageCache)

            do {
                 resultDeleteCars = try self.managedObjectContext.execute(requestdeleteCars)
                 resultDeleteImagecache = try self.managedObjectContext.execute(requestdeleteImageCache)

                try self.managedObjectContext.save()
                
            } catch (let error) {
                print("Error deleting Data from Database \(error.localizedDescription) : The Number : \(String(describing: resultDeleteCars?.description)) \(String(describing: resultDeleteImagecache?.description))")
                let userInfoDictionary = ["error": "Error deleteing all Car data from Database"] as [AnyHashable : Any]
                NotificationCenter.default.post(name: dataModelNotification , object: nil, userInfo: userInfoDictionary)
                
            }
            
            NotificationCenter.default.post(name: dataModelNotification , object: nil)
            
        }
        
    }
    
    public func convertRemoteCarsToLocal(carsToSave: [CarRemote])  {
        
        
        let resturnedErrors = carsToSave.compactMap { (oneCarToSave) -> () in
        
            self.managedObjectContext.performAndWait {
                let newCarEntity = Car(context: managedObjectContext)
                newCarEntity.id = oneCarToSave.id
                newCarEntity.modelIdentifier = oneCarToSave.modelIdentifier
                newCarEntity.modelName = oneCarToSave.modelName
                newCarEntity.name = oneCarToSave.name
                newCarEntity.group = oneCarToSave.group.map { $0.rawValue }
                newCarEntity.color = oneCarToSave.color
                newCarEntity.series = oneCarToSave.series
                
                newCarEntity.fuelType = oneCarToSave.fuelType.map { $0.rawValue }
                newCarEntity.fuellevel = Double(oneCarToSave.fuelType?.rawValue ?? "00.00") ?? 00.00
                newCarEntity.transmission = oneCarToSave.transmission.map { $0.rawValue }
                newCarEntity.licensePlate = oneCarToSave.licensePlate
                newCarEntity.latitude = Double(oneCarToSave.latitude ?? 00.00)
                newCarEntity.longitude = Double(oneCarToSave.longitude ?? 00.00)
                newCarEntity.innerCleanliness = oneCarToSave.innerCleanliness.map { $0.rawValue }
                newCarEntity.carImageUrl = oneCarToSave.carImageURL
                // Call Async Code to Save Car Image
                if let urlString = newCarEntity.carImageUrl, let modelid = newCarEntity.modelIdentifier {
                    self.loadImageIntoCache(image:urlString, model: modelid)
                }
                
                do {
                   try self.managedObjectContext.save()
                } catch let error as NSError {
                    print("Error Saving Car: \(error.localizedDescription)")
                    let userInfoDictionary = ["error": "Error saving Car to Database"] as [AnyHashable : Any]
                    NotificationCenter.default.post(name: dataModelNotification , object: nil, userInfo: userInfoDictionary)
                    return
                }
            }
        }
        print("Finished Saving : \(resturnedErrors.debugDescription)")
        NotificationCenter.default.post(name: dataModelNotification , object: nil, userInfo: nil)

    }
    
    // Reusable Fetch for Core Data
    private func fetchAndReloadCars()  -> [Car] {
        
        var theCars = [Car]()
        let fetchRequest = NSFetchRequest<Car>()
        let carEntity = NSEntityDescription.entity(forEntityName: String(describing: Car.self), in: managedObjectContext)
        fetchRequest.entity = carEntity
        
        do {
            theCars = try managedObjectContext.fetch(fetchRequest)
            print(" Cars count is: \(String(describing: theCars.count))")
        } catch let error as NSError {
            print(" Core data Error is : \(error)")
            let userInfoDictionary = ["error": "Error fatching Cars from Database"] as [AnyHashable : Any]
            NotificationCenter.default.post(name: dataModelNotification , object: nil, userInfo: userInfoDictionary)

        }
        
        return theCars
        
    }
    
    public func getTotalNumberOfCarsInDb() -> Int {
        
        let fetchRequest = NSFetchRequest<NSNumber>(entityName: String(describing: Car.self))
        fetchRequest.resultType = .countResultType
        
        let returned = try? managedObjectContext.fetch(fetchRequest)
        
        return returned?.first as? Int ?? 0
        
    }
    
    private func getFromImageCache(withId Id: String) -> CarImageCache? {
        
        let fetchRequest = NSFetchRequest<CarImageCache>(entityName: String(describing: CarImageCache.self))
        fetchRequest.predicate = NSPredicate(format: "%K == %@", #keyPath(CarImageCache.modelIdentifier), Id)
        fetchRequest.resultType = .managedObjectResultType
        
        let theCarImageCacheObject = try? managedObjectContext.fetch(fetchRequest)
        
        print("theCarImageCacheObject is : \(String(describing: theCarImageCacheObject?.first?.modelIdentifier)) ")
        
        return theCarImageCacheObject?.first
        
    }

    
    private func doesTheModelIdAlreadyExistInImageCache(withId Id: String) -> Bool {
        
        let fetchRequest = NSFetchRequest<NSNumber>(entityName: String(describing: CarImageCache.self))
        fetchRequest.predicate = NSPredicate(format: "%K == %@", #keyPath(CarImageCache.modelIdentifier), Id)
        fetchRequest.resultType = .countResultType
        
        let returned = try? managedObjectContext.fetch(fetchRequest)
        
        let itemsFound = returned?.first as? Int ?? 0
        
        print("Car Id is : \(Id) and number found is : \(itemsFound)")
        return itemsFound > 0
        
    }
    
    // Downloading of Image to external file through Core Data
    
    private func saveAnImageIntoCache(withImageData theData: Data, theStringURL url: String, andId theId: String)  {
        
        self.managedObjectContext.performAndWait {
            let newCarEntity = CarImageCache(context: managedObjectContext)
            newCarEntity.modelIdentifier = theId
            newCarEntity.carImageUrl = url
            newCarEntity.imageStore = theData
            
            do {
               try self.managedObjectContext.save()
                let mapViewFullUpdate = self.dataTasksDownloadingCurrently.count == 0 ? "fullMapViewUpdate" : ""
                let userInfoDictionary = ["imageCacheUpdate": mapViewFullUpdate] as [AnyHashable : Any]?
                print("Sending imageCacheUpdate Notification : \(self.dataTasksDownloadingCurrently.count)")
                NotificationCenter.default.post(name: dataModelNotification , object: nil, userInfo: userInfoDictionary)
            } catch let error as NSError {
                print("Error Saving to ImageCache: \(error.localizedDescription)")
                let userInfoDictionary = ["error": "Error saving and Image to Database"] as [AnyHashable : Any]
                NotificationCenter.default.post(name: dataModelNotification , object: nil, userInfo: userInfoDictionary)
                return
            }
        }
        
    }
    
    private func loadImageIntoCache(image imageToDownload: String, model modelId: String)  {
        
    if let url = URL(string: imageToDownload) {
        if doesTheModelIdAlreadyExistInImageCache(withId: modelId) {
            return
        } else {
                    self.getData(from: url) { (data, response, error) in
                         self.dataTasksDownloadingCurrently.removeFirst()
                        if error == nil, let data = data, (response as? HTTPURLResponse)?.statusCode == 200 {
                            print("saving image data : \(String(describing: data)) response \(String(describing: (response as? HTTPURLResponse)?.statusCode)) error \(String(describing: error))")
                            self.saveAnImageIntoCache(withImageData: data, theStringURL: imageToDownload, andId: modelId)
                        } else {
                            let userInfoDictionary = ["imageCacheUpdate": "fullMapViewUpdate"] as [AnyHashable : Any]
                            NotificationCenter.default.post(name: dataModelNotification , object: nil, userInfo: userInfoDictionary)
                        }
                    }
            }
        }
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        let dataTask = URLSession.shared.dataTask(with: url, completionHandler: completion)
        dataTasksDownloadingCurrently.append(dataTask)
        dataTask.resume()
    }

    // DataStore Functions
    
    private var theCarsCurrentlyToDisplay = [Car]()
    
    func reloadAllCarsIntoArray() -> Int {
        theCarsCurrentlyToDisplay.removeAll()
        theCarsCurrentlyToDisplay.append(contentsOf: fetchAndReloadCars())
        return theCarsCurrentlyToDisplay.count
    }

    func getAllCarsInStore() -> Int {
        return theCarsCurrentlyToDisplay.count
    }
    
    func returnACarAtAnIndexPath(row: Int) -> Car? {
        guard let _ = theCarsCurrentlyToDisplay.first else {
            return nil
        }
        let theCar =  theCarsCurrentlyToDisplay[row] as Car
        
        return theCar
    }
    
    func retrieveImageForACarFrom(modelId: String) -> Data? {
        
        return getFromImageCache(withId: modelId)?.imageStore
        
    }
    
    func createAnnotationsForMap() -> [CarAnnotation] {
        
        return theCarsCurrentlyToDisplay.compactMap { (theCar) -> CarAnnotation in
            let coords = CLLocationCoordinate2D(latitude: theCar.latitude, longitude: theCar.longitude)
            let imageBinary = getFromImageCache(withId: theCar.modelIdentifier ?? "")?.imageStore
            let theCarAnnotation = CarAnnotation(title: theCar.modelIdentifier  ?? "", locationName: theCar.name ?? "", coordinate: coords, image: imageBinary)
            return theCarAnnotation
        }
    }
    
}
