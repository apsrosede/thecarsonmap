//
//  carAnnotation.swift
//  theCarsOnAMap
//
//  Created by AdrianRoseMacBookAir on 01.11.19.
//  Copyright © 2019 apsrose. All rights reserved.
//

import Foundation
import MapKit


class CarAnnotation: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    let imageBinary: Data?
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D, image: Data?) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        self.imageBinary = image
        
        super.init()
    }

}
