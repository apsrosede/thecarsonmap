//
//  carAnnotationView.swift
//  theCarsOnAMap
//
//  Created by AdrianRose on 02.11.19.
//  Copyright © 2019 apsrose. All rights reserved.
//

import UIKit
import MapKit

//class carAnnotationView: MKAnnotationView {
//
//    /*
//    // Only override draw() if you perform custom drawing.
//    // An empty implementation adversely affects performance during animation.
//    override func draw(_ rect: CGRect) {
//        // Drawing code
//    }
//    */
//
//}

class carAnnotationView: MKAnnotationView {

  override var annotation: MKAnnotation? {
    willSet {
        var imageView : UIImage?
    
      guard let car  = newValue as? CarAnnotation else {return}

      canShowCallout = true
      calloutOffset = CGPoint(x: -5, y: 5)
      let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
        size: CGSize(width: 80, height: 50)))
      if let data = car.imageBinary {
         // Backend image downloaded
          imageView = UIImage(data: data) // ,scale: .maximum(60.00, 60.00
      } else {
          imageView = UIImage(systemName: "car")
        }
      // imageView
      mapsButton.setBackgroundImage(imageView!, for: UIControl.State())

        rightCalloutAccessoryView = mapsButton
      // rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        
//        if let data = car.imageBinary {
//            image = UIImage(data: data,scale: .maximum(60.00, 60.00) )
//        } else {
        image = UIImage(systemName: "car.fill")
//                .withRenderingMode(.alwaysOriginal)
//                    .colorized(color: UIColor.blue)

//        }
        
        
//      if let imageName = artwork.imageName {
//        image = UIImage(named: imageName)
//      } else {
//        image = nil
//      }

      let detailLabel = UILabel()
      detailLabel.numberOfLines = 0
      detailLabel.font = detailLabel.font.withSize(12)
        detailLabel.text = car.locationName
      detailCalloutAccessoryView = detailLabel
    }
  }

}


extension UIImage {

    func colorized(color : UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height);
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0);
        let context = UIGraphicsGetCurrentContext();
        context!.setBlendMode(.multiply)
        context?.draw(self.cgImage!, in: rect)
        context?.clip(to: rect, mask: self.cgImage!)
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let colorizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return colorizedImage ?? UIImage()
    }
}
