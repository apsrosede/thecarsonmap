//
//  CodableAndNetworking.swift
//  theCarsOnAMap
//
//  Created by AdrianRoseMacBookAir on 29.10.19.
//  Copyright © 2019 apsrose. All rights reserved.
//


import Foundation

// MARK: - Element
 internal struct CarRemote: Codable {
    let id, modelIdentifier, modelName, name: String?
    let make, group: Group?
    let color, series: String?
    let fuelType: FuelType?
    let fuelLevel: Double?
    let transmission: Transmission?
    let licensePlate: String?
    let latitude, longitude: Double?
    let innerCleanliness: InnerCleanliness?
    let carImageURL: String?

    enum CodingKeys: String, CodingKey {
        case id, modelIdentifier, modelName, name, make, group, color, series, fuelType, fuelLevel, transmission, licensePlate, latitude, longitude, innerCleanliness
        case carImageURL = "carImageUrl"
    }
}

enum FuelType: String, Codable {
    case d = "D"
    case e = "E"
    case p = "P"
}

enum Group: String, Codable {
    case bmw = "BMW"
    case mini = "MINI"
}

enum InnerCleanliness: String, Codable {
    case clean = "CLEAN"
    case regular = "REGULAR"
    case veryClean = "VERY_CLEAN"
}

enum Transmission: String, Codable {
    case a = "A"
    case m = "M"
}

typealias Cars = [Car]

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
    return encoder
}

// MARK: - URLSession response handlers

public extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }

     internal func getCarsTask(with url: URL, completionHandler: @escaping ([CarRemote]?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
    internal func setUpTheNetwork(completionClosure: @escaping  networkDataIntoLocalStoreClosure)  {
        
        let url = URL(string: "https://cdn.sixt.io/codingtask/cars")!
        self.getCarsTask(with: url ) { (cars, response, error) in
            
            print(" complete :\(String(describing: cars?.count))")
            
            let carsTemp = cars!
            
            completionClosure(carsTemp)
            
        }.resume()

    }
}
