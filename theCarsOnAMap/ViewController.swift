//
//  ViewController.swift
//  theCarsOnAMap
//
//  Created by AdrianRoseMacBookAir on 29.10.19.
//  Copyright © 2019 apsrose. All rights reserved.
//

import UIKit
import CoreData
import MapKit

class ViewController: UIViewController {
    
    var theDataManager: dataToNetworkManager?
    var theLastCacheUpdate : NSDate? = NSDate()
    let twoMinutesHammeringProtection = TimeInterval(2.0 * 60.0)
    let theTableViewCellIdentifier = "CarsTableViewCell"

    var carsToPlot: [CarAnnotation] = []
    let regionRadius: CLLocationDistance = 4000
    
    @IBOutlet weak var theTableView: UITableView!
    @IBOutlet weak var theMapView: MKMapView!
    @IBOutlet weak var theActivitySpinner: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theTableView.refreshControl = UIRefreshControl()
        theTableView.refreshControl?.addTarget(self, action: #selector(RefreshAllData(_:)), for: .valueChanged)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleANotificationFromDataLayer(_:)), name: dataModelNotification, object: nil)
        
        setUpCoreData()
        
        if theDataManager?.getTotalNumberOfCarsInDb() == 0 {
            setUpNetworkConnection()
        } else {
            // load from Store
            let carsInStore = theDataManager?.reloadAllCarsIntoArray()
            print("Cars in Store : \(String(describing: carsInStore))")
            setUpMapView()
        }
        
        theTableView.dataSource = self
        theTableView.delegate = self


    }
    
    
    func setUpMapView() {
        //print("setUpMapView called")
        theMapView.delegate = self
        theMapView.register(carAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        guard let count = theDataManager?.getAllCarsInStore(), count > 0, let annotations = theDataManager?.createAnnotationsForMap()  else {
            return
        }
        carsToPlot = annotations
        
        //print("setUpMapView called with annotations number :\(annotations.count)")
        
        guard let firstCoordinate = carsToPlot.first?.coordinate else {
            return
        }
        let initialLocation = CLLocation(latitude: firstCoordinate.latitude, longitude: firstCoordinate.longitude)
        centerMapOnLocation(location: initialLocation)
        theMapView.addAnnotations(carsToPlot)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
      theMapView.setRegion(coordinateRegion, animated: true)
    }
    
    // To Do
    func doAFullNetworkReload() {
    }
    
    // To Do
    @IBAction func RefreshAllData(_ sender: UIBarButtonItem) {
        
//        guard let futureDateWhenWeCanUpdate = theLastCacheUpdate?.addingTimeInterval(twoMinutesHammeringProtection), let theLastUpdate = theLastCacheUpdate else {
//            theTableView.refreshControl?.endRefreshing()
//            return
//        }
//
//        let doWeRefreshData = futureDateWhenWeCanUpdate.timeIntervalSince(theLastUpdate as Date)
//
//        guard doWeRefreshData >= twoMinutesHammeringProtection else {
//            theTableView.refreshControl?.endRefreshing()
//            print("can only refresh data every 2 Minutes")
//            // else AlertView - Data updated 2 Minutes ago - Can only update every 2 Minutes
//            return
//        }

        DispatchQueue.main.async {
            
            self.theTableView.setContentOffset(CGPoint(x: 0, y: self.theTableView.contentOffset.y - (self.theTableView.refreshControl?.frame.size.height ?? 10.00)), animated: true)

            self.theTableView.refreshControl?.beginRefreshing()

            self.theDataManager?.deleteAllData()
            self.theMapView.removeAnnotations(self.carsToPlot)
    
            self.theTableView.refreshControl?.endRefreshing()
        }
        
    }
    
    @objc func handleANotificationFromDataLayer(_ notification:Notification) {
       
        
        if let fullMapViewUpdate = notification.userInfo?["imageCacheUpdate"] as? String {
            let _ = self.theDataManager?.reloadAllCarsIntoArray()
            print("imageCacheUpdate")
            // Recreate Annotation Views
            // How many cars in Store
            print("Full Update MapView is \(fullMapViewUpdate)")
            if fullMapViewUpdate == "fullMapViewUpdate" {
                 print("Downloads are: \(String(describing: theDataManager?.dataTasksDownloadingCurrently.count))")
                 DispatchQueue.main.async {
                self.setUpMapView()
                self.theTableView.reloadData()
                self.stopSpinner()
                }
            }
        } else if let errorType = notification.userInfo?["error"] as? String {
            self.displayAlertWithError(theErrorReceived: errorType)
            print("Error received is: \(errorType)")
            self.stopSpinner()
        } else {
            let carsInStore = self.theDataManager?.reloadAllCarsIntoArray()
            print("Cars in Store : \(String(describing: carsInStore))")
            DispatchQueue.main.async {
            self.theTableView.reloadData()
            self.stopSpinner()
            }
         }
          
    }
    
    func displayAlertWithError(theErrorReceived: String) {
        
        DispatchQueue.main.async {
            
             let alert = UIAlertController(title: "Sorry an Error Occurred", message: theErrorReceived, preferredStyle: .alert)

             alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

             self.present(alert, animated: true)
        }
    }

    
    func setUpCoreData() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        theDataManager = dataToNetworkManager(theContextToSaveInto: managedContext)
        
    }
        
    func setUpNetworkConnection() {
        
        startSpinner()
        
        URLSession.shared.setUpTheNetwork { carsToPersist in
            
            guard let _ = carsToPersist.first else {
                return }
            
            // Delete Cars and Image Cache
            
            self.theDataManager?.convertRemoteCarsToLocal(carsToSave: carsToPersist)
            self.theLastCacheUpdate = NSDate()
            self.stopSpinner()
        }
    }
    
    func startSpinner()  {
        DispatchQueue.main.async {
            self.theActivitySpinner.startAnimating()
        }
    }
    
    func stopSpinner()  {
        DispatchQueue.main.async {
            if self.theActivitySpinner.isAnimating {
                self.theActivitySpinner.stopAnimating()
            }
        }
    }

}

extension ViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return theDataManager?.getAllCarsInStore() ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: theTableViewCellIdentifier, for: indexPath) as? carDisplayTableViewCell
        
        let aCarFromStore = theDataManager?.returnACarAtAnIndexPath(row: indexPath.row)
                
        cell?.name.text = aCarFromStore?.name
        cell?.modelName.text = aCarFromStore?.modelName
        cell?.color.text = aCarFromStore?.color
        cell?.fuelLevel.text = String(aCarFromStore?.fuellevel ?? 00.00)
        cell?.fuelType.text = aCarFromStore?.fuelType
        cell?.licensePlate.text = aCarFromStore?.licensePlate
        
        if let modelId = aCarFromStore?.modelIdentifier, let data = theDataManager?.retrieveImageForACarFrom(modelId: modelId) {
            let imageFromStore = UIImage(data: data)
            cell?.carImageToDisplay?.image = imageFromStore
        }
        
        return cell ?? carDisplayTableViewCell()
        
    }

}

extension ViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

extension ViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is CarAnnotation else { return nil }

        let identifier = "Annotation"
        print("\(#function)")
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? carAnnotationView

        if annotationView == nil {
            annotationView = carAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }

        return annotationView
    }

    
}

