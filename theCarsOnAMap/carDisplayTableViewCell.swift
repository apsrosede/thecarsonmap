//
//  carDisplayTableViewCell.swift
//  theCarsOnAMap
//
//  Created by AdrianRose on 31.10.19.
//  Copyright © 2019 apsrose. All rights reserved.
//

import UIKit

class carDisplayTableViewCell: UITableViewCell {
    
    // Cells
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var modelName: UILabel!
    @IBOutlet weak var color: UILabel!
    @IBOutlet weak var licensePlate: UILabel!
    @IBOutlet weak var fuelType: UILabel!
    @IBOutlet weak var fuelLevel: UILabel!
    @IBOutlet weak var carImageToDisplay: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        carImageToDisplay.image = UIImage(systemName: "car.fill")
    }

}
