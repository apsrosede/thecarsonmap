//
//  theCarsOnAMapTests.swift
//  theCarsOnAMapTests
//
//  Created by AdrianRoseMacBookAir on 29.10.19.
//  Copyright © 2019 apsrose. All rights reserved.
//

import XCTest
import CoreData
@testable import theCarsOnAMap

class theCarsOnAMapTests: XCTestCase {
    
    var theDataToNetworkManagerForTesting: dataToNetworkManager?
    
    lazy var theXCTestStorePersistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "theCarsOnAMap")
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
            print("Core data Store : \(String(describing: storeDescription.url))")
        })
        return container
    }()


    func saveContextTotheXCTestStorePersistentContainer () {
        let context = theXCTestStorePersistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }


    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFirstTestToSeeIfXCTestOk() {
        XCTAssertNotNil("inMemoryStore", "Core Data Container Name cannot be nil")
    }
    
    func testStoreExists()  {
        var testStoreName : String?
        testStoreName = theXCTestStorePersistentContainer.name
        XCTAssertNotNil(testStoreName, "Store has Name: \(String(describing: testStoreName))")
    }
    
    func testDataToNetworkManagerInit() {
        let dataContext = theXCTestStorePersistentContainer.viewContext
        theDataToNetworkManagerForTesting = dataToNetworkManager(theContextToSaveInto: dataContext)
        XCTAssertNotNil(theDataToNetworkManagerForTesting, "Core Data Context cannot be nil")
    }
    
    func testNumberOfCarsInDatabaseLocally() {
        let dataContext = theXCTestStorePersistentContainer.viewContext
        theDataToNetworkManagerForTesting = dataToNetworkManager(theContextToSaveInto: dataContext)
        let numberOfCarsLocally = theDataToNetworkManagerForTesting?.getTotalNumberOfCarsInDb()
        XCTAssertEqual(numberOfCarsLocally, 0)
    }
    
    func testNumberOfImagesInThecacheLocally() {
        let dataContext = theXCTestStorePersistentContainer.viewContext
        theDataToNetworkManagerForTesting = dataToNetworkManager(theContextToSaveInto: dataContext)
        let theImage = theDataToNetworkManagerForTesting?.retrieveImageForACarFrom(modelId: "0")
        XCTAssertNil(theImage, "There should be no images in the Core Data in Memory Store")
    }
    
    func testTheNetworkCallToServerForCars() {
        let theSession = URLSession.shared
        let theNetworkCallExpectation = expectation(description: "callToNetworkServerForCarList")
        
        theSession.setUpTheNetwork { carsToPersist in
            
           let theCarsCount = carsToPersist.count
           XCTAssertGreaterThan(theCarsCount,1,"There should be more than 1 car downloadable from Server")
           theNetworkCallExpectation.fulfill()
        }
        
         waitForExpectations(timeout: 8) { (error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }

    
}
